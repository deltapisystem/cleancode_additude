﻿using System;
using Demo.NotDip.Business;
using Demo.NotDip.Database;

namespace Demo.NotDip.ApplicationRoot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var service = new CustomerService(new CustomerRepository());
            var customer = new Customer {Id = Guid.NewGuid()};
            service.CreateCustomerAndDoSomeMoreStuff(customer);

        }
    }
}