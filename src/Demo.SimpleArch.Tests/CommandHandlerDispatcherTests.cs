﻿using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;
using Demo.SimpleArch.Tests.Fakes;
using NSubstitute;
using NUnit.Framework;

namespace Demo.SimpleArch.Tests
{
    [TestFixture]
    public class CommandHandlerDispatcherTests
    {
        private IResolveCommandHandler _resolveCommandHandler;
        private CommandHandlerDispatcher _sut;
        private SimpleLogger _logger;

        [SetUp]
        public void Setup()
        {
            _logger = new SimpleLogger();
            _resolveCommandHandler = Substitute.For<IResolveCommandHandler>();
            _sut = new CommandHandlerDispatcher(_resolveCommandHandler) { Logger = _logger };
        }

        [Test]
        public async Task CommandHandlerDispatcher_Execute_Ok()
        {
            // arrange
            _resolveCommandHandler.Resolve<FakeCommand>().Returns(new FakeCommandHandler());
            var fakeCommand = new FakeCommand();

            // act
            var result = await _sut.ExecuteAsync(fakeCommand);

            // assert
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);

            // assert that we had some logging
            Assert.IsFalse(string.IsNullOrWhiteSpace(_logger.ToString()));
        }

        [Test]
        public async Task CommandHandlerDispatcher_Returns_Internal_Server_Error_For_Missing_Handler()
        {
            // arrange
            _resolveCommandHandler.Resolve<FakeCommand>().Returns((FakeCommandHandler)null);
            var fakeCommand = new FakeCommand();

            // act
            var result = await _sut.ExecuteAsync(fakeCommand);

            // assert
            Assert.AreEqual(HttpStatusCode.InternalServerError, result.StatusCode);
        }
    }
}
