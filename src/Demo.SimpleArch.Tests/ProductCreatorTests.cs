﻿using System;
using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.CreateProduct;
using Demo.SimpleArch.Data;
using Demo.SimpleArch.Database;
using NSubstitute;
using NUnit.Framework;

namespace Demo.SimpleArch.Tests
{
    [TestFixture]
    public class ProductCreatorTests
    {
        private ProductCreator _sut;
        private IStorageClient _storageClient;

        [SetUp]
        public void SetUp()
        {
            _storageClient = Substitute.For<IStorageClient>();

            _sut = new ProductCreator
            {
                StorageClientFactory = new FakeStorageFactoryClient(_storageClient),
                AuditResolver = new DefaultAuditResolver(),
                TimeMaster = new TimeMaster()
            };
        }

        [Test]
        public async Task ProductCreator_Returns_No_Content_When_Company_Is_Not_Found()
        {
            // arrange
            _storageClient.GetCompanyWithoutProductsAsync(Guid.NewGuid()).ReturnsForAnyArgs((Company)null);
            var command = new CreateProductCommand
                {
                    Id = Guid.NewGuid(),
                    CompanyId = Guid.NewGuid(),
                    Name = "My product"
                };

            // act
            var result = await _sut.ExecuteAsync(command);

            // assert
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }

        [Test]
        public async Task ProductCreator_Can_Create_Product()
        {
            // arrange
            var company = DatabaseHelper.CreateCompanies(DateTimeOffset.Now, "ASSA", 0);
            _storageClient.GetCompanyWithoutProductsAsync(company.Id).Returns(company);
            _storageClient.CreateProductAsync(null, null).ReturnsForAnyArgs(new StoreResult<Product>(new Product(), HttpStatusCode.Created));

            var command = new CreateProductCommand
            {
                Id = Guid.NewGuid(),
                CompanyId = company.Id,
                Name = "My product"
            };

            // act
            var result = await _sut.ExecuteAsync(command);

            // assert
            Assert.AreEqual(HttpStatusCode.Created, result.StatusCode);
        }
    }
}
