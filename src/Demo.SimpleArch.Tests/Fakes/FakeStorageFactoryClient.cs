﻿using Demo.SimpleArch.Data;

namespace Demo.SimpleArch.Tests
{
    public class FakeStorageFactoryClient : IStorageClientFactory
    {
        private readonly IStorageClient _client;

        public FakeStorageFactoryClient(IStorageClient client)
        {
            _client = client;
        }

        public IStorageClient CreateStorageClient()
        {
            return _client;
        }
    }
}