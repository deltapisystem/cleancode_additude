﻿using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.Tests.Fakes
{
    public class FakeCommandHandler : IHandleCommand<FakeCommand>
    {
        public async Task<CommandResult> ExecuteAsync(FakeCommand command)
        {
            return await Task.Run(() => new CommandResult(HttpStatusCode.OK));
        }
    }
}