﻿using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.Tests.Fakes
{
    public class FakeCommand : ICommand
    {
        public string MyData { get; set; }
    }
}
