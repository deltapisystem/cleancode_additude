﻿using System;
using System.Linq;
using Demo.WhatYouNeed;
using NSubstitute;
using NUnit.Framework;

namespace Demo.SimpleArch.Tests
{
    [TestFixture]
    public class WhatYouNotNeedServiceTests
    {
        [Test]
        public void NoFunAtAllUnitTest()
        {
            // arrange
            var logger = Substitute.For<ILogger>();
            var orderRepo = Substitute.For<IOrderRepository>();
            var customerRepo = Substitute.For<ICustomerRepository>();
            var validator = Substitute.For<IOrderValdator>();
            var emailService = Substitute.For<IEmailService>();
            var inventoryService = Substitute.For<IInventoryService>();

            var sut = new WhatYouNotNeedService(logger, orderRepo, customerRepo, validator, emailService, inventoryService);
            var order = new Order
            {
                CustomerId = Guid.NewGuid(),
                ShippingAddressId = Guid.NewGuid()
            };
            var orderRow = new OrderRow { ArticleId = Guid.NewGuid(), Quantity = 23 };
            order.OrderRows.Add(orderRow);

            var customer = new Customer
            {
                Email = "daniel@test.com",
                Id = order.CustomerId,
                Name = "Daniel"
            };
            customer.ShippingAddresses.Add(new Address { Id = order.ShippingAddressId });

            // mock so customer is fetched
            customerRepo.GetDetailedCustomer(order.CustomerId).Returns(customer);

            // mock so order is valid
            validator.IsOrderValid(order).Returns(true);

            // mock so article is reserved at inventory
            inventoryService.ReserveArticle(order.OrderRows.First().ArticleId, order.OrderRows.First().Quantity).Returns(orderRow.Quantity);

            // mock so order is updated
            orderRepo.UpdateOrder(order).Returns(true);

            // act
            var result = sut.HandleOrder(order);

            // assert
            Assert.IsTrue(result);

            // verify so email service is called
            emailService.ReceivedWithAnyArgs(1).SendHtmlMail(string.Empty, Enumerable.Empty<string>());
        }
    }
}
