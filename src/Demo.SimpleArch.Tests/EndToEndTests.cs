﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Demo.SimpleArch.Api.Controllers;
using Demo.SimpleArch.Api.Input;
using Demo.SimpleArch.Api.Output;
using Demo.SimpleArch.Commands;
using Demo.SimpleArch.Database;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Demo.SimpleArch.Tests
{
    [TestFixture]
    public class EndToEndTests
    {
        private CompanyController _sutCompany;
        private ProductController _sutProduct;
        private StorageClient _context;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var container = ApiTestHelper.CreateEndToEndTestContainer();

            _sutCompany = new CompanyController(container.Resolve<ICreateContext>())
            {
                CommandDispatcher = container.Resolve<ICommandDispatcher>(),
                Request = ApiTestHelper.CreateRequest(HttpMethod.Post, "http://localhost.com/v1/companies"),
                Url = ApiTestHelper.MockUrlHelper("http://localhost.com/v1/companies"),
                TimeMaster = new TimeMaster()
            };

            _sutProduct = new ProductController(container.Resolve<ICreateContext>())
            {
                CommandDispatcher = container.Resolve<ICommandDispatcher>(),
                Request = ApiTestHelper.CreateRequest(HttpMethod.Post, "http://localhost.com/v1/products"),
                Url = ApiTestHelper.MockUrlHelper("http://localhost.com/v1/products"),
                TimeMaster = new TimeMaster()
            };

            _context = ApiTestHelper.CreateTestDatabase(container.Resolve<IAppLogger>());
            _context.CreateDefaultData(_sutCompany.TimeMaster.GetUtcNow(), 100);
        }

        [Test]
        public async Task CompanyController_GetPagedCompanies_Last_Page_Returns_Ok()
        {
            // arrange - count data
            var numberOfManufactures = await _context.Companies.CountAsync().ConfigureAwait(false);
            Assert.IsTrue(numberOfManufactures > 0);

            // act
            var responseTask = await _sutCompany.GetPagedCompanies(new PageFilter { Skip = numberOfManufactures - 10, Take = 10 });
            var result = await responseTask.ExecuteAsync(CancellationToken.None).Result.Content.ReadAsAsync<PagedCompaniesResponse>();

            // assert
            Assert.AreEqual(10, result.Companies.Count);
            Assert.IsFalse(result.HasMoreResult);
        }

        [Test]
        public async Task CompanyController_GetCompany_Returns_Company()
        {
            // arrange - count data
            var expected = _context.Companies.First(t => t.Products.Count > 0);

            // act
            var responseTask = await _sutCompany.GetCompany(expected.Id);
            var response = responseTask.ExecuteAsync(CancellationToken.None).Result;
            var company = await response.Content.ReadAsAsync<CompanyResponse>();

            // assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(expected.Id, company.Id);
            Assert.AreEqual(expected.Name, company.Name);
            Assert.AreEqual(expected.LastUpdated, company.LastUpdated);
            Assert.AreEqual(expected.Products.Count, company.Products.Count);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(company.LogoUrl));
        }

        [Test]
        public async Task CompanyController_GetCompany_Returns_No_Content()
        {
            // arrange - count data

            // act
            var responseTask = await _sutCompany.GetCompany(Guid.NewGuid());
            var response = responseTask.ExecuteAsync(CancellationToken.None);

            // assert
            Assert.AreEqual(HttpStatusCode.NoContent, response.Result.StatusCode);
        }

        [Test]
        public async Task CompanyController_Create_Company_Ok()
        {
            // arrange
            var logo = Convert.FromBase64String(DatabaseHelper.Png);
            var model = new CreateCompanyModel
            {
                Name = "ElitFönster AB",
                VatNr = Guid.NewGuid().ToString(),
                OrgNr = Guid.NewGuid().ToString(),
                Logo = logo
            };

            // act
            var responseTask = await _sutCompany.CreateCompany(model);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);
            var createdItem = await response.Content.ReadAsAsync<CompanyResponse>();

            // assert
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(response.Headers.Location.ToString()));

            // get stored item from database
            var storedItem = _context.Companies.FromSql("SELECT TOP 1 * FROM Companies WHERE Name = @name", new SqlParameter("@name", model.Name)).First();

            // assert returned company
            Assert.AreEqual(model.Name, createdItem.Name);
            Assert.AreEqual(storedItem.LastUpdated, createdItem.LastUpdated);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(createdItem.LogoUrl));
            Assert.IsEmpty(createdItem.Products);
            Assert.IsNotNull(createdItem.RowVersion);
            Assert.AreEqual(model.VatNr, createdItem.VatNr);
            Assert.AreEqual(model.OrgNr, createdItem.OrgNr);

            // assert stored company
            Assert.AreEqual(model.Name, storedItem.Name);
            Assert.AreEqual(Constants.UserId, storedItem.CreatedBy);
            Assert.AreEqual(storedItem.Created, storedItem.LastUpdated);
            Assert.AreEqual(storedItem.LastUpdatedBy, storedItem.CreatedBy);
            Assert.AreEqual(logo, storedItem.Logo);
            Assert.IsEmpty(storedItem.Products);
            Assert.IsNotNull(storedItem.RowVersion);

            // assert stored audit
            var storedAudit = await _context.Audits.SingleAsync(t => t.EntityId == storedItem.Id).ConfigureAwait(false);
            Assert.AreEqual("C", storedAudit.Action);
            Assert.AreEqual(storedItem.LastUpdatedBy, storedAudit.ChangedBy);
            Assert.AreEqual("COMPANY", storedAudit.EntityType);
            Assert.AreEqual(JsonConvert.SerializeObject(storedItem), storedAudit.EntityJson);
        }

        [Test]
        public async Task CompanyController_Update_Company_Ok()
        {
            // arrange
            var existing = _context.Companies.First(t => t.Products.Count > 0);
            var model = new UpdateCompanyModel
            {
                Name = existing.Name + "Updated",
                RowVersion = existing.RowVersion
            };

            // act
            var responseTask = await _sutCompany.UpdateCompany(existing.Id, model);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);

            // assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            _context.Entry(existing).Reload();
            var storedItem = await _context.Companies.Include(t => t.Products).SingleAsync(t => t.Name == model.Name)
                .ConfigureAwait(false);

            // assert company
            Assert.AreEqual(model.Name, storedItem.Name);
            Assert.AreEqual(Constants.UserId, storedItem.CreatedBy);
            Assert.IsTrue(storedItem.LastUpdated > storedItem.Created);
            Assert.AreEqual(existing.Products.Count, storedItem.Products.Count);
            Assert.AreEqual(existing.RowVersion, storedItem.RowVersion);
            Assert.AreEqual(existing.Products.Count, storedItem.Products.Count);

            // assert audit
            var storedAudit = await _context.Audits.SingleOrDefaultAsync(t => t.EntityId == storedItem.Id && t.Action == "U").ConfigureAwait(false);
            Assert.IsNotNull(storedAudit);
            Assert.AreEqual(storedItem.LastUpdatedBy, storedAudit.ChangedBy);
            Assert.AreEqual("COMPANY", storedAudit.EntityType);
        }

        [Test]
        public async Task CompanyController_Update_Company_Conflict()
        {
            // arrange
            var existing = _context.Companies.First(t => t.Products.Count > 0);
            var model = new UpdateCompanyModel
            {
                Name = existing.Name + "Updated",
                RowVersion = Convert.FromBase64String("ZmRmZHNm")
            };

            // act
            var responseTask = await _sutCompany.UpdateCompany(existing.Id, model);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);

            // assert
            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public async Task CompanyController_Delete_Company_And_Products_Ok()
        {
            // arrange
            var existing = _context.Companies.First(t => t.Products.Count > 0);
            var model = new DeleteCompanyModel
            {
                RowVersion = existing.RowVersion
            };

            // act
            var responseTask = await _sutCompany.DeleteCompany(existing.Id, model);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);

            // assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var deleted = _context.Companies.FromSql("SELECT TOP 1 * FROM Companies WHERE Id = @id", new SqlParameter("@id", existing.Id)).FirstOrDefault();
            Assert.IsNull(deleted);
            var products = _context.Products.FromSql("SELECT * FROM Products WHERE CompanyId = @id", new SqlParameter("@id", existing.Id)).ToList();
            Assert.AreEqual(0, products.Count);
        }

        [Test]
        public async Task CompanyController_Delete_Company_Conflict()
        {
            // arrange
            var existing = _context.Companies.First(t => t.Products.Count > 0);
            var model = new DeleteCompanyModel
            {
                RowVersion = Convert.FromBase64String("ZmRmZHNm")
            };

            // act
            var responseTask = await _sutCompany.DeleteCompany(existing.Id, model);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);

            // assert
            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public async Task ProductController_Create_Product_Ok()
        {
            // arrange
            var company = _context.Companies.First(t => t.Products.Count == 0);
            var model = new CreateProductModel { Name = "ElitFönster Normal 80 x 60 cm" };

            // act
            var responseTask = await _sutProduct.CreateProduct(company.Id, model);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);
            var createdItem = await response.Content.ReadAsAsync<ProductResponse>();

            // assert
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.IsTrue(!string.IsNullOrWhiteSpace(response.Headers.Location.ToString()));

            var storedItem = await _context.Products.SingleAsync(t => t.Name == model.Name)
                .ConfigureAwait(false);

            // assert stored product
            Assert.AreEqual(model.Name, storedItem.Name);
            Assert.AreEqual(Constants.UserId, storedItem.CreatedBy);
            Assert.AreEqual(storedItem.Created, storedItem.LastUpdated);
            Assert.AreEqual(storedItem.LastUpdatedBy, storedItem.CreatedBy);
            Assert.IsNotNull(storedItem.RowVersion);

            // assert returned product
            Assert.AreEqual(model.Name, createdItem.Name);
            Assert.AreEqual(storedItem.LastUpdated, createdItem.LastUpdated);
            Assert.IsNotNull(createdItem.RowVersion);
            Assert.IsNotNull(storedItem.RowVersion);

            // assert audit
            var storedAudit = await _context.Audits.SingleAsync(t => t.EntityId == storedItem.Id).ConfigureAwait(false);
            Assert.AreEqual("C", storedAudit.Action);
            Assert.AreEqual(storedItem.LastUpdatedBy, storedAudit.ChangedBy);
            Assert.AreEqual("PRODUCT", storedAudit.EntityType);
            Assert.AreEqual(JsonConvert.SerializeObject(storedItem), storedAudit.EntityJson);
        }

        [Test]
        public async Task ProductController_GetProduct_Returns_Product()
        {
            // arrange
            var companyWithProduct = _context.Companies.First(t => t.Products.Count > 0);
            var expected = companyWithProduct.Products.Last();

            // act
            var responseTask = await _sutProduct.GetProduct(companyWithProduct.Id, expected.Id);
            var response = responseTask.ExecuteAsync(CancellationToken.None).Result;
            var product = await response.Content.ReadAsAsync<ProductResponse>();

            // assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(expected.Id, product.Id);
            Assert.AreEqual(expected.Name, product.Name);
            Assert.AreEqual(expected.LastUpdated, product.LastUpdated);
            Assert.IsNotNull(expected.RowVersion);
        }

        [Test]
        public async Task ProductController_GetPagedProducts_Returns_Correct()
        {
            // arrange - count data
            var companiesWithProducts = await _context.Companies.Include(m => m.Products).Where(t => t.Products.Count > 2).ToListAsync().ConfigureAwait(false);
            var company = companiesWithProducts.OrderByDescending(t => t.Products.Count).First();

            // act
            var responseTask = await _sutProduct.GetProductsForCompany(company.Id, new PageFilter { Skip = 0, Take = 2 });
            var result = await responseTask.ExecuteAsync(CancellationToken.None).Result.Content.ReadAsAsync<PagedProductsResponse>();

            // assert
            Assert.AreEqual(2, result.Products.Count);
            Assert.IsTrue(result.HasMoreResult);
            Assert.AreEqual(result.CompanyId, company.Id);
        }

        [Test]
        public async Task ProductController_Update_Product_Ok()
        {
            // arrange
            var company = _context.Companies.First(t => t.Products.Count > 0);
            var existing = company.Products.Last();
            var model = new UpdateProductModel
            {
                Name = existing.Name + "Updated",
                RowVersion = existing.RowVersion
            };

            // act
            var responseTask = await _sutProduct.UpdateProduct(company.Id, existing.Id, model);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);

            // assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            _context.Entry(existing).Reload();
            var storedItem = await _context.Products.SingleAsync(t => t.Name == model.Name)
                .ConfigureAwait(false);

            // assert company
            Assert.AreEqual(model.Name, storedItem.Name);
            Assert.AreEqual(Constants.UserId, storedItem.CreatedBy);
            Assert.IsTrue(storedItem.LastUpdated > storedItem.Created);
            Assert.AreEqual(existing.RowVersion, storedItem.RowVersion);

            // assert audit
            var storedAudit = await _context.Audits.SingleOrDefaultAsync(t => t.EntityId == storedItem.Id && t.Action == "U").ConfigureAwait(false);
            Assert.IsNotNull(storedAudit);
            Assert.AreEqual(storedItem.LastUpdatedBy, storedAudit.ChangedBy);
            Assert.AreEqual("PRODUCT", storedAudit.EntityType);
        }

        [Test]
        public async Task ProductController_Update_Product_Conflict()
        {
            // arrange
            var company = _context.Companies.First(t => t.Products.Count > 0);
            var existing = company.Products.Last();
            var model = new UpdateProductModel
            {
                Name = existing.Name + "Updated",
                RowVersion = Convert.FromBase64String("ZmRmZHNm")
            };

            // act
            var responseTask = await _sutProduct.UpdateProduct(company.Id, existing.Id, model);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);

            // assert
            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public async Task ProductController_Delete_Product_Ok()
        {
            // arrange
            var company = _context.Companies.First(t => t.Products.Count > 0);
            var existing = company.Products.Last();

            // act
            var responseTask = await _sutProduct.DeleteProduct(company.Id, existing.Id);
            var response = await responseTask.ExecuteAsync(CancellationToken.None);

            // assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var products = _context.Products.FromSql("SELECT * FROM Products WHERE id = @id", new SqlParameter("@id", existing.Id)).ToList();
            Assert.AreEqual(0, products.Count);

            // assert audit
            var storedAudit = await _context.Audits.SingleOrDefaultAsync(t => t.EntityId == existing.Id && t.Action == "D").ConfigureAwait(false);
            Assert.IsNotNull(storedAudit);
            Assert.AreEqual("PRODUCT", storedAudit.EntityType);
        }
    }
}