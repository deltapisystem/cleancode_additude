﻿using System.Net;
using NUnit.Framework;

namespace Demo.SimpleArch.Tests
{
    [TestFixture]
    public class ExtensionTests
    {
        [TestCase(HttpStatusCode.OK)]
        [TestCase(HttpStatusCode.Created)]
        public void Extension_Is_Created_True_For_Ok_And_Created(HttpStatusCode code)
        {
            Assert.IsTrue(code.IsCreated());
        }

        [TestCase(HttpStatusCode.Accepted)]
        [TestCase(HttpStatusCode.InternalServerError)]
        [TestCase(HttpStatusCode.NoContent)]
        [TestCase(HttpStatusCode.NotFound)]
        [TestCase(HttpStatusCode.BadRequest)]
        public void Extension_Is_Created_False(HttpStatusCode code)
        {
            Assert.IsFalse(code.IsCreated());
        }
    }
}
