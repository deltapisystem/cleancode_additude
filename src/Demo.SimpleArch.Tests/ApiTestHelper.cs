﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using Autofac;
using Demo.SimpleArch.Api;
using Demo.SimpleArch.Api.Storage;
using Demo.SimpleArch.Database;
using NSubstitute;

namespace Demo.SimpleArch.Tests
{
    public static class ApiTestHelper
    {
        public static string GetTestDbConnection()
        {
            return ConfigurationManager.AppSettings["DbConnection"];
        }

        public static StorageClient CreateTestDatabase(IAppLogger logger)
        {
            try
            {
                var context = new ContextFactory(logger, GetTestDbConnection()).WriteContext();
                context.Database.EnsureCreated();
                context.ClearDatabase();
                return context;
            }
            catch (Exception e)
            {
                logger.LogException(e);
                throw;
            }
        }

        public static HttpRequestMessage CreateRequest(HttpMethod httpMethod, string url)
        {
            var config = new HttpConfiguration();

            config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            var request = new HttpRequestMessage(httpMethod, url);
            request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
            request.Properties[HttpPropertyKeys.HttpRouteDataKey] = new HttpRouteData(new HttpRoute());
            return request;
        }

        public static IContainer CreateEndToEndTestContainer()
        {
            var builder = DependencyContainerRegistrations.RegisterToContainerBuilder(new HttpConfiguration());
            return builder.Build();
        }

        public static UrlHelper MockUrlHelper(string url)
        {
            var urlHelper = Substitute.For<UrlHelper>();
            urlHelper.Link(null, Arg.Any<IDictionary<string, string>>()).ReturnsForAnyArgs(url);
            return urlHelper;
        }
    }
}