﻿using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;
using Demo.Dip.Business;

namespace Demo.Dip.Database
{
    public class SqlCustomerRepository : ICustomerServiceNeedsHelpWithDatabase
    {
        public void StoreCustomer(DipCustomer customer)
        {
            new SqlConnection().Insert(customer);
        }
    }
}
