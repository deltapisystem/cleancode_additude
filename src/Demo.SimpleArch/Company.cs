﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.SimpleArch
{
    public class Company
    {
        public Company()
        {
            Name = string.Empty;
            Logo = null;
            Products = new List<Product>();
            OrgNr = string.Empty;
            VatNr = string.Empty;
        }

        [JsonIgnore]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("orgNr")]
        public string OrgNr { get; set; }

        [JsonProperty("vatNr")]
        public string VatNr { get; set; }

        [JsonIgnore]
        public byte[] Logo { get; set; }

        [JsonIgnore]
        public DateTimeOffset Created { get; set; }

        [JsonIgnore]
        public Guid CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset LastUpdated { get; set; }

        [JsonIgnore]
        public Guid LastUpdatedBy { get; set; }

        [JsonProperty("products")]
        public IList<Product> Products { get; set; }

        [JsonIgnore]
        public byte[] RowVersion { get; set; }

        public bool HasLogo()
        {
            return Logo != null;
        }
    }
}