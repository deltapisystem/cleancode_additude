﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.UpdateProduct
{
    public class ProductUpdater : Handle<UpdateProductCommand>
    {
        public override async Task<CommandResult> ExecuteAsync(UpdateProductCommand command)
        {
            using (var client = StorageClientFactory.CreateStorageClient())
            {
                var company = await client.GetCompanyWithProductAsync(command.CompanyId, command.ProductId).ConfigureAwait(false);
                if (company == null || company.Products.Count == 0)
                {
                    return new CommandResult(HttpStatusCode.NoContent);
                }

                var product = company.Products.First();
                product.Name = command.Name;
                product.LastUpdated = TimeMaster.GetUtcNow();
                product.LastUpdatedBy = AuditResolver.GetCurrentUserId();
                var result = await client.UpdateProductAsync(product, command.RowVersion);
                return new CommandResult(result.StatusCode);
            }
        }
    }
}