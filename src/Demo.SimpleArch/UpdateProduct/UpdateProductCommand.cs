﻿using System;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.UpdateProduct
{
    public class UpdateProductCommand : ICommand
    {
        public UpdateProductCommand()
        {
            Name = string.Empty;
        }

        public string Name { get; set; }

        public Guid ProductId { get; set; }

        public Guid CompanyId { get; set; }

        public byte[] RowVersion { get; set; }
    }
}