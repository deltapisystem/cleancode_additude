﻿using System.Net;

namespace Demo.SimpleArch
{
    public static class Extensions
    {
        public static bool IsCreated(this HttpStatusCode code)
        {
            return code == HttpStatusCode.Created || code == HttpStatusCode.OK;
        }
    }
}
