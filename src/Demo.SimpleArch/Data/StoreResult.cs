﻿using System.Net;

namespace Demo.SimpleArch.Data
{
    public class StoreResult<T>
    where T : class
    {
        public StoreResult(T item, HttpStatusCode statusCode)
        {
            Item = item;
            StatusCode = statusCode;
        }

        public T Item { get; }

        public HttpStatusCode StatusCode { get; }
    }
}