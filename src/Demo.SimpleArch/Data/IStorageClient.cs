﻿using System;
using System.Threading.Tasks;

namespace Demo.SimpleArch.Data
{
    public interface IStorageClient : IDisposable
    {
        Task<Company> GetCompanyWithProductAsync(Guid companyId, Guid productId);

        Task<Company> GetCompanyWithoutProductsAsync(Guid id);

        Task<StoreResult<Company>> CreateCompanyAsync(Company company);

        Task<StoreResult<Company>> UpdateCompanyAsync(Company company, byte[] rowVersion);

        Task<StoreResult<Company>> DeleteCompanyAsync(Company company, byte[] rowVersion);

        Task<StoreResult<Product>> CreateProductAsync(Company company, Product product);

        Task<StoreResult<Product>> UpdateProductAsync(Product product, byte[] rowVersion);

        Task<StoreResult<Product>> DeleteProductAsync(Product product);
    }
}