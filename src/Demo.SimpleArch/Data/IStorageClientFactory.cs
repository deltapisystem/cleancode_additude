﻿namespace Demo.SimpleArch.Data
{
    public interface IStorageClientFactory
    {
        IStorageClient CreateStorageClient();
    }
}
