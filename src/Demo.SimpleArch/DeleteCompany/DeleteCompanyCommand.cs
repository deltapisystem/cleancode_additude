﻿using System;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.DeleteCompany
{
    public class DeleteCompanyCommand : ICommand
    {
        public Guid Id { get; set; }

        public byte[] RowVersion { get; set; }
    }
}