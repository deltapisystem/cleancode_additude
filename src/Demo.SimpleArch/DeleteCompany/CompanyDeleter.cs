﻿using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.DeleteCompany
{
    public class CompanyDeleter : Handle<DeleteCompanyCommand>
    {
        public override async Task<CommandResult> ExecuteAsync(DeleteCompanyCommand command)
        {
            using (var client = StorageClientFactory.CreateStorageClient())
            {
                var company = await client.GetCompanyWithoutProductsAsync(command.Id).ConfigureAwait(false);
                if (company == null)
                {
                    return new CommandResult(HttpStatusCode.NoContent);
                }

                var result = await client.DeleteCompanyAsync(company, command.RowVersion);
                return new CommandResult(result.StatusCode);
            }
        }
    }
}