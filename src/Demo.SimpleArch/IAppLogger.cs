﻿using System;

namespace Demo.SimpleArch
{
    public interface IAppLogger
    {
        void LogInfo(string message);

        void Trace(string message);

        void LogError(string message);

        void LogException(Exception ex);
    }
}
