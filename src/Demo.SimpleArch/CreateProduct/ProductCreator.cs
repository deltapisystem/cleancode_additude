﻿using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.CreateProduct
{
    public class ProductCreator : Handle<CreateProductCommand>
    {
        public override async Task<CommandResult> ExecuteAsync(CreateProductCommand command)
        {
            var now = TimeMaster.GetUtcNow();
            var userId = AuditResolver.GetCurrentUserId();

            var itemToCreate = new Product()
            {
                Id = command.Id,
                Name = command.Name,
                Created = now,
                CreatedBy = userId,
                LastUpdated = now,
                LastUpdatedBy = userId
            };

            using (var client = StorageClientFactory.CreateStorageClient())
            {
                var company = await client.GetCompanyWithoutProductsAsync(command.CompanyId).ConfigureAwait(false);
                if (company == null)
                {
                    return new CommandResult(HttpStatusCode.NoContent);
                }

                var result = await client.CreateProductAsync(company, itemToCreate).ConfigureAwait(false);
                return new CommandResult(result.StatusCode);
            }
        }
    }
}