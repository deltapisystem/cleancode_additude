﻿using System;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.CreateProduct
{
    public class CreateProductCommand : ICommand
    {
        public CreateProductCommand()
        {
            Name = string.Empty;
        }

        public string Name { get; set; }

        public Guid Id { get; set; }

        public Guid CompanyId { get; set; }
    }
}