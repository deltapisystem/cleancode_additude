﻿using System;
using System.Globalization;

namespace Demo.SimpleArch
{
    public class TimeMaster : ITimeMaster
    {
        public const string DefaultUtcFormat = "yyyy-MM-ddTHH:mm:ss.fffZ";

        public static string ToUtcFormat(DateTimeOffset dt)
        {
            return dt.ToString(DefaultUtcFormat, CultureInfo.InvariantCulture);
        }

        public DateTimeOffset GetUtcNow()
        {
            return DateTimeOffset.UtcNow;
        }
    }
}