﻿using System;
using Newtonsoft.Json;

namespace Demo.SimpleArch
{
    public class Product
    {
        public Product()
        {
            Name = string.Empty;
        }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonIgnore]
        public DateTimeOffset Created { get; set; }

        [JsonIgnore]
        public Guid CreatedBy { get; set; }

        [JsonIgnore]
        public DateTimeOffset LastUpdated { get; set; }

        [JsonIgnore]
        public Guid LastUpdatedBy { get; set; }

        [JsonIgnore]
        public byte[] RowVersion { get; set; }

        [JsonIgnore]
        public virtual Company Company { get; set; }
    }
}