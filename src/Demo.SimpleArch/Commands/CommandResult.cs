﻿using System.Net;

namespace Demo.SimpleArch.Commands
{
    public class CommandResult
    {
        public CommandResult(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
            ErrorMessage = string.Empty;
        }

        public HttpStatusCode StatusCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}
