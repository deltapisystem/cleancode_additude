﻿using System.Threading.Tasks;

namespace Demo.SimpleArch.Commands
{
    public interface ICommandDispatcher
    {
        IAppLogger Logger { get; set; }

        Task<CommandResult> ExecuteAsync<T>(T command)
            where T : ICommand;
    }
}
