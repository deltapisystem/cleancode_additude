﻿using System.Threading.Tasks;

namespace Demo.SimpleArch.Commands
{
    // ReSharper disable once TypeParameterCanBeVariant - should not be variant
    public interface IHandleCommand<TCommand>
        where TCommand : ICommand
    {
        Task<CommandResult> ExecuteAsync(TCommand command);
    }
}
