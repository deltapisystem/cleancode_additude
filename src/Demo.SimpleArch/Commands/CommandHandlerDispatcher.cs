﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace Demo.SimpleArch.Commands
{
    public class CommandHandlerDispatcher : ICommandDispatcher
    {
        private readonly IResolveCommandHandler _resolver;

        public CommandHandlerDispatcher(IResolveCommandHandler resolver)
        {
            _resolver = resolver ?? throw new ArgumentNullException(nameof(resolver));
        }

        public IAppLogger Logger { get; set; }

        public async Task<CommandResult> ExecuteAsync<T>(T command)
            where T : ICommand
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            Logger.Trace("Command will be executed");
            try
            {
                IHandleCommand<T> handler = _resolver.Resolve<T>();
                var result = await handler.ExecuteAsync(command).ConfigureAwait(false);
                Logger.Trace("Command executed");
                return result;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
                return new CommandResult(HttpStatusCode.InternalServerError);
            }
        }
    }
}