﻿namespace Demo.SimpleArch.Commands
{
    public interface IResolveCommandHandler
    {
        IHandleCommand<T> Resolve<T>()
            where T : ICommand;
    }
}