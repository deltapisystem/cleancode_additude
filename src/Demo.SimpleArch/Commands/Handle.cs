﻿using System.Threading.Tasks;
using Demo.SimpleArch.Data;

namespace Demo.SimpleArch.Commands
{
    public abstract class Handle<T> : IHandleCommand<T>
        where T
        : class, ICommand
    {
        public ITimeMaster TimeMaster { get; set; }

        public IAuditResolver AuditResolver { get; set; }

        public IStorageClientFactory StorageClientFactory { get; set; }

        public abstract Task<CommandResult> ExecuteAsync(T command);
    }
}