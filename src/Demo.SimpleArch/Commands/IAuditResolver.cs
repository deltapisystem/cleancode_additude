﻿using System;

namespace Demo.SimpleArch.Commands
{
    public interface IAuditResolver
    {
        Guid GetCurrentUserId();
    }
}
