﻿using System;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch
{
    public class DefaultAuditResolver : IAuditResolver
    {
        public Guid GetCurrentUserId()
        {
            return Constants.UserId;
        }
    }
}
