﻿using System;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.UpdateCompanyLogo
{
    public class UpdateCompanyLogoCommand : ICommand
    {
        public byte[] Image { get; set; }

        public Guid ManufactureId { get; set; }
    }
}
