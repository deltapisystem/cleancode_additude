﻿using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.UpdateCompanyLogo
{
    public class LogoUpdater : Handle<UpdateCompanyLogoCommand>
    {
        public override async Task<CommandResult> ExecuteAsync(UpdateCompanyLogoCommand command)
        {
            using (var client = StorageClientFactory.CreateStorageClient())
            {
                var company = await client.GetCompanyWithoutProductsAsync(command.ManufactureId);
                if (company == null)
                {
                    return new CommandResult(HttpStatusCode.NoContent);
                }

                company.Logo = command.Image;
                var result = await client.UpdateCompanyAsync(company, company.RowVersion);
                return new CommandResult(result.StatusCode);
            }
        }
    }
}