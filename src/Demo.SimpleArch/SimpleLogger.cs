﻿using System;
using System.Diagnostics;
using System.Text;

namespace Demo.SimpleArch
{
    public class SimpleLogger : IAppLogger
    {
        private readonly StringBuilder _sb = new StringBuilder();

        public void LogInfo(string message)
        {
            LogIt(message);
        }

        public void Trace(string message)
        {
            LogIt(message);
        }

        public void LogError(string message)
        {
            LogIt(message);
        }

        public void LogException(Exception ex)
        {
            LogIt(ex.ToString());
        }

        public override string ToString()
        {
            return _sb.ToString();
        }

        private void LogIt(string message)
        {
           var formatted = $"[{TimeMaster.ToUtcFormat(DateTimeOffset.UtcNow)}] - {message}";
            Debug.WriteLine(formatted);
            _sb.AppendLine(formatted);
        }
    }
}