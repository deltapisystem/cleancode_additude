﻿using System;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.CreateCompany
{
    public class CreateCompanyCommand : ICommand
    {
        public CreateCompanyCommand()
        {
            Name = string.Empty;
            VatNr = string.Empty;
            OrgNr = string.Empty;
        }

        public string Name { get; set; }

        public string VatNr { get; set; }

        public string OrgNr { get; set; }

        public Guid Id { get; set; }

        public byte[] Logo { get; set; }
    }
}