﻿using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.CreateCompany
{
    public class CompanyCreator : Handle<CreateCompanyCommand>
    {
        private readonly IVerifyCompany _verifier;

        public CompanyCreator(IVerifyCompany verifier)
        {
            _verifier = verifier;
        }

        public override async Task<CommandResult> ExecuteAsync(CreateCompanyCommand command)
        {
            var verifyResult = _verifier.VerifyCompany(command.OrgNr, command.VatNr);
            if (verifyResult.StatusCode != HttpStatusCode.OK)
            {
                _verifier.SendCompanyVerificationMail(command, verifyResult);
                return verifyResult;
            }

            var now = TimeMaster.GetUtcNow();
            var userId = AuditResolver.GetCurrentUserId();

            var itemToCreate = new Company
            {
                Id = command.Id,
                Name = command.Name,
                Logo = command.Logo,
                VatNr = command.VatNr,
                OrgNr = command.OrgNr,
                Created = now,
                CreatedBy = userId,
                LastUpdated = now,
                LastUpdatedBy = userId
            };

            using (var client = StorageClientFactory.CreateStorageClient())
            {
                var result = await client.CreateCompanyAsync(itemToCreate).ConfigureAwait(false);
                return new CommandResult(result.StatusCode);
            }
        }
    }
}