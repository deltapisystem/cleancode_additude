﻿using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.CreateCompany
{
    public interface IVerifyCompany
    {
        CommandResult VerifyCompany(string orgNr, string vatNr);

        void SendCompanyVerificationMail(CreateCompanyCommand model, CommandResult verificationResult);
    }
}