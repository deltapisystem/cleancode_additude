﻿using System;

namespace Demo.SimpleArch
{
    public class Audit
    {
        public Audit()
        {
            Action = string.Empty;
            EntityJson = string.Empty;
        }

        public long Id { get; set; }

        public Guid EntityId { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public Guid ChangedBy { get; set; }

        public string Action { get; set; }

        public string EntityJson { get; set; }

        public string EntityType { get; set; }
    }
}