﻿using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.UpdateCompany
{
    public class CompanyUpdater : Handle<UpdateCompanyCommand>
    {
        public override async Task<CommandResult> ExecuteAsync(UpdateCompanyCommand command)
        {
            using (var client = StorageClientFactory.CreateStorageClient())
            {
                var company = await client.GetCompanyWithoutProductsAsync(command.Id).ConfigureAwait(false);
                if (company == null)
                {
                    return new CommandResult(HttpStatusCode.NoContent);
                }

                company.Name = command.Name;
                company.LastUpdated = TimeMaster.GetUtcNow();
                company.LastUpdatedBy = AuditResolver.GetCurrentUserId();

                var result = await client.UpdateCompanyAsync(company, command.RowVersion);
                return new CommandResult(result.StatusCode);
            }
        }
    }
}