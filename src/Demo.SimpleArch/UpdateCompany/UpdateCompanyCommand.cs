﻿using System;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.UpdateCompany
{
    public class UpdateCompanyCommand : ICommand
    {
        public UpdateCompanyCommand()
        {
            Name = string.Empty;
        }

        public string Name { get; set; }

        public Guid Id { get; set; }

        public byte[] RowVersion { get; set; }
    }
}