﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.DeleteProduct
{
    public class ProductDeleter : Handle<DeleteProductCommand>
    {
        public override async Task<CommandResult> ExecuteAsync(DeleteProductCommand command)
        {
            using (var client = StorageClientFactory.CreateStorageClient())
            {
                var company = await client.GetCompanyWithProductAsync(command.CompanyId, command.ProductId).ConfigureAwait(false);
                if (company == null || !company.Products.Any())
                {
                    return new CommandResult(HttpStatusCode.NoContent);
                }

                var result = await client.DeleteProductAsync(company.Products.Single(t => t.Id == command.ProductId));
                return new CommandResult(result.StatusCode);
            }
        }
    }
}