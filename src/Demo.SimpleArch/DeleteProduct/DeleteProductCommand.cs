﻿using System;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.DeleteProduct
{
    public class DeleteProductCommand : ICommand
    {
        public Guid CompanyId { get; set; }

        public Guid ProductId { get; set; }
    }
}