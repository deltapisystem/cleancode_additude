﻿using System;

namespace Demo.SimpleArch
{
    public interface ITimeMaster
    {
        DateTimeOffset GetUtcNow();
    }
}
