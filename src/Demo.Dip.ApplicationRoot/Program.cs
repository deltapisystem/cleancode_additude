﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.Dip.Business;
using Demo.Dip.Database;

namespace Demo.Dip.ApplicationRoot
{
    public class Program
    {
        static void Main(string[] args)
        {
            var service = new DipCustomerService(new SqlCustomerRepository());
            var customer = new DipCustomer();
            service.PublishCustomer(customer);
        }
    }
}
