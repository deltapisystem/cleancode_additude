﻿namespace Demo.Dip.Business
{
    public interface ICustomerServiceNeedsHelpWithDatabase
    {
        void StoreCustomer(DipCustomer customer);
    }
}
