﻿namespace Demo.Dip.Business
{
    public class DipCustomerService
    {
        private readonly ICustomerServiceNeedsHelpWithDatabase _lowLevelClient;

        public DipCustomerService(ICustomerServiceNeedsHelpWithDatabase lowLevelClient)
        {
            _lowLevelClient = lowLevelClient;
        }

        public void PublishCustomer(DipCustomer customer)
        {
            _lowLevelClient.StoreCustomer(customer);
        }
    }
}