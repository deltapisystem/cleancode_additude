﻿using System.Collections.Generic;

namespace Demo.NotNull
{
    public class NullEntity
    {
        public ICollection<int> ListThatCanBeNull { get;}

        public string EntityName { get; set; }
    }
}
