﻿using System.Collections;
using System.Collections.Generic;

namespace Demo.NotNull
{
    public class NotNullEntity
    {
        public NotNullEntity()
        {
            NeverNull = new List<int>();
            EntityName = string.Empty;
        }

        public ICollection NeverNull { get; }

        public string EntityName { get; set; }
    }
}
