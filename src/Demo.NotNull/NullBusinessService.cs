﻿using System.Globalization;

namespace Demo.NotNull
{
    public class Service
    {
        public NullEntity DoSomethingWithNotCleanEntity(NullEntity nullEntity)
        {
            if (nullEntity.ListThatCanBeNull != null)
            {
                if (nullEntity.EntityName == null)
                {
                    nullEntity.EntityName = string.Empty;
                }

                nullEntity.EntityName += nullEntity.ListThatCanBeNull.Count.ToString(CultureInfo.InvariantCulture);
            }

            return nullEntity;
        }

        public NotNullEntity DoSomethingWithCleanEntity(NotNullEntity notNullEntity)
        {
            notNullEntity.EntityName += notNullEntity.NeverNull.Count.ToString(CultureInfo.InvariantCulture);
            return notNullEntity;
        }
    }
}