﻿namespace Demo.WhatYouNeed
{
    public interface IOrderValdator
    {
        bool IsOrderValid(Order order);
    }
}