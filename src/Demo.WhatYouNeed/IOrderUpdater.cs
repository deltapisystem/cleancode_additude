﻿using System;

namespace Demo.WhatYouNeed
{
    public interface IOrderUpdater
    {
        void UpdateOrder(Order detailedOrderOrder, DateTimeOffset utcNow);
    }
}