﻿using System.Collections.Generic;

namespace Demo.WhatYouNeed
{
    public interface IEmailService
    {
        void SendHtmlMail(string html, IEnumerable<string> recipients);
    }
}