﻿using System;
using System.Collections.Generic;

namespace Demo.WhatYouNeed
{
    public class Order
    {
        public Order()
        {
            OrderRows = new List<OrderRow>();
        }

        public Guid CustomerId { get; set; }

        public IList<OrderRow> OrderRows { get; set; }

        public Guid OrderId { get; set; }

        public DateTimeOffset MailSent { get; set; }

        public string MailSentTo { get; set; }

        public DateTimeOffset OrderHandled { get; set; }

        public string OrderState { get; set; }

        public Guid ShippingAddressId { get; set; }
    }
}
