﻿namespace Demo.WhatYouNeed
{
    public class DetailedOrder
    {
        public Order Order { get; set; }

        public Customer Customer { get; set; }

        public Address ShipTo { get; set; }
    }
}
