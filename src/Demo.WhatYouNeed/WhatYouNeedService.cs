﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.WhatYouNeed
{
    public class WhatYouNeedService
    {
        private readonly IPlaceOrderService _orderService;
        private readonly IEmailService _emailService;

        public WhatYouNeedService(IPlaceOrderService orderService, IEmailService emailService)
        {
            _orderService = orderService;
            _emailService = emailService;
        }

        public bool HandleOrder(DetailedOrder detailedOrder, Customer customer)
        {
            ValidateOrder(detailedOrder, customer);
            _orderService.ReserveArticlesAndCreateBackOrderWhenNeeded(detailedOrder.Order, detailedOrder.ShipTo);
            SendOrderConfirmationEmail(detailedOrder);
            _orderService.UpdateOrder(detailedOrder.Order, DateTimeOffset.UtcNow);
            return true;
        }

        internal void SendOrderConfirmationEmail(DetailedOrder order)
        {
            var html = "<html><h1>Order confirmation</h1></html>";
            _emailService.SendHtmlMail(html, new List<string> { order.Customer.Email });
        }

        private static void ValidateOrder(DetailedOrder detailedOrder, Customer customer)
        {
            if (customer == null)
            {
                throw new OrderException("Customer is not valid for order!");
            }

            if (customer.ShippingAddresses.All(t => t.Id != detailedOrder.ShipTo.Id))
            {
                throw new OrderException("Customer shipping address is not stored on customer!");
            }

            if (!detailedOrder.Order.OrderRows.Any())
            {
                throw new OrderException("Missing order rows!");
            }
        }
    }
}