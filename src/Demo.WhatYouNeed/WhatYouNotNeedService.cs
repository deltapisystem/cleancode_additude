﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;

namespace Demo.WhatYouNeed
{
    public class WhatYouNotNeedService
    {
        private readonly ILogger _logger;
        private readonly IOrderRepository _orderRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderValdator _orderValidator;
        private readonly IEmailService _emailService;
        private readonly IInventoryService _inventoryService;

        public WhatYouNotNeedService(ILogger logger, IOrderRepository orderRepository, ICustomerRepository customerRepository, IOrderValdator orderValidator, IEmailService emailSender, IInventoryService inventoryService)
        {
            _logger = logger;
            _orderRepository = orderRepository;
            _customerRepository = customerRepository;
            _orderValidator = orderValidator;
            _orderValidator = orderValidator;
            _emailService = emailSender;
            _inventoryService = inventoryService;
        }

        public bool HandleOrder(Order order)
        {
            if (order.OrderId == Guid.Empty)
            {
                order.OrderId = Guid.NewGuid();
            }

            var customer = _customerRepository.GetDetailedCustomer(order.CustomerId);
            if (customer == null)
            {
                _logger.LogError($"Customer with id {order.CustomerId} was not found!");
                return false;
            }

            var shippingAddress = customer.ShippingAddresses.FirstOrDefault(t => t.Id == order.ShippingAddressId);
            if (shippingAddress == null)
            {
                _logger.LogError(
                    $"Shipping address {order.ShippingAddressId} for customer {customer.Id} was not found.");
                return false;
            }

            if (!_orderValidator.IsOrderValid(order))
            {
                _logger.LogError($"Order {order.OrderId} was not valid.");
                return false;
            }

            foreach (var orderRow in order.OrderRows)
            {
                var result = _inventoryService.ReserveArticle(orderRow.ArticleId, orderRow.Quantity);
                if (result != orderRow.Quantity)
                {
                    _logger.LogError($"Article {orderRow.ArticleId} could not be reserved!");
                    orderRow.BackOrder = true;
                }
            }

            var html = CreateOrderConfirmationEmail(order, customer);
            _emailService.SendHtmlMail(html, new List<string> { customer.Email });
            order.MailSent = DateTimeOffset.UtcNow;
            order.MailSentTo = customer.Email;
            order.OrderHandled = DateTimeOffset.UtcNow;
            order.OrderState = "Verified";
            var updateResult = _orderRepository.UpdateOrder(order);
            if (!updateResult)
            {
                throw new OrderException($"Order {order.OrderId} could not be updated.");
            }

            return true;
        }

        public Order DoSomethingForAnotherInvokerWithOnlyOneSomeDependencies(Guid orderId, Guid customerId)
        {
            var order = _orderRepository.GetOrder(orderId);
            if (order.CustomerId == customerId)
            {
                return order;
            }

            throw new SecurityException();
        }

        private string CreateOrderConfirmationEmail(Order order, Customer customer)
        {
            return "<html><h1>Order confirmation</h1></html>";
        }
    }
}