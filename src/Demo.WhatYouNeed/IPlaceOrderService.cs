﻿namespace Demo.WhatYouNeed
{
    public interface IPlaceOrderService : IOrderUpdater
    {
        void ReserveArticlesAndCreateBackOrderWhenNeeded(Order detailedOrderOrder, Address detailedOrderShipTo);
    }
}
