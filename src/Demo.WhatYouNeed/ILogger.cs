﻿namespace Demo.WhatYouNeed
{
    public interface ILogger
    {
        void LogError(string message);
    }
}
