﻿using System;

namespace Demo.WhatYouNeed
{
    public interface ICustomerRepository
    {
        Customer GetDetailedCustomer(Guid orderCustomerId);
    }
}