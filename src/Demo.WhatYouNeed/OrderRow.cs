﻿using System;

namespace Demo.WhatYouNeed
{
    public class OrderRow
    {
        public Guid ArticleId { get; set; }

        public int Quantity { get; set; }

        public bool BackOrder { get; set; }
    }
}