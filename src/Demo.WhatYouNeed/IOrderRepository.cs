﻿using System;

namespace Demo.WhatYouNeed
{
    public interface IOrderRepository
    {
        bool UpdateOrder(Order order);

        Order GetOrder(Guid orderId);
    }
}