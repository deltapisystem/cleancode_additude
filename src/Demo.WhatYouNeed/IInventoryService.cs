﻿using System;

namespace Demo.WhatYouNeed
{
    public interface IInventoryService
    {
        int ReserveArticle(Guid articleId, int quantity);
    }
}