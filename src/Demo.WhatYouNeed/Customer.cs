﻿using System;
using System.Collections.Generic;

namespace Demo.WhatYouNeed
{
    public class Customer
    {
        public Customer()
        {
            Email = string.Empty;
            Name = string.Empty;
            ShippingAddresses = new List<Address>();
        }

        public string Email { get; set; }

        public string Name { get; set; }

        public ICollection<Address> ShippingAddresses { get; }

        public Guid Id { get; set; }
    }
}
