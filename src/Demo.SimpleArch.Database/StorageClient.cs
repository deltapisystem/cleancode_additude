﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Demo.SimpleArch.Data;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Database
{
    public class StorageClient : DbContext, IStorageClient
    {
        public StorageClient(IAppLogger logger, DbContextOptions<StorageClient> options)
            : base(options)
        {
            Logger = logger;
        }

        public IAppLogger Logger { get; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Audit> Audits { get; set; }

        public async Task<Company> GetCompanyWithProductAsync(Guid companyId, Guid productId)
        {
            var company = await Companies.FirstOrDefaultAsync(m => m.Id == companyId);
            if (company == null)
            {
                return null;
            }

            var product = await Entry(company)
                .Collection(s => s.Products)
                .Query()
                .FirstOrDefaultAsync(p => p.Id == productId);

            return product == null ? null : company;
        }

        public async Task<Company> GetCompanyWithoutProductsAsync(Guid id)
        {
            return await Companies.FirstOrDefaultAsync(t => t.Id == id).ConfigureAwait(false);
        }

        public async Task<StoreResult<Company>> CreateCompanyAsync(Company company)
        {
            Companies.Add(company);
            Audits.Add(CreateAudit(company, "C", JsonConvert.SerializeObject(company)));
            var result = await CommitAsync().ConfigureAwait(false);
            return new StoreResult<Company>(company, result);
        }

        public async Task<StoreResult<Company>> UpdateCompanyAsync(Company company, byte[] rowVersion)
        {
            Entry(company).State = EntityState.Modified;
            Entry(company).OriginalValues["RowVersion"] = rowVersion;
            Audits.Add(CreateAudit(company, "U", JsonConvert.SerializeObject(company)));
            var result = await CommitAsync().ConfigureAwait(false);
            return new StoreResult<Company>(company, result);
        }

        public async Task<StoreResult<Company>> DeleteCompanyAsync(Company company, byte[] rowVersion)
        {
            Companies.Remove(company);
            Entry(company).OriginalValues["RowVersion"] = rowVersion;
            Audits.Add(CreateAudit(company, "D", string.Empty));
            var result = await CommitAsync().ConfigureAwait(false);
            return new StoreResult<Company>(company, result);
        }

        public async Task<StoreResult<Product>> CreateProductAsync(Company company, Product product)
        {
            Products.Add(product);
            company.Products.Add(product);
            Audits.Add(CreateAudit(product, "C", JsonConvert.SerializeObject(product)));
            var result = await CommitAsync().ConfigureAwait(false);
            return new StoreResult<Product>(product, result);
        }

        public async Task<StoreResult<Product>> UpdateProductAsync(Product product, byte[] rowVersion)
        {
            Entry(product).State = EntityState.Modified;
            Entry(product).OriginalValues["RowVersion"] = rowVersion;
            Audits.Add(CreateAudit(product, "U", JsonConvert.SerializeObject(product)));
            var result = await CommitAsync().ConfigureAwait(false);
            return new StoreResult<Product>(product, result);
        }

        public async Task<StoreResult<Product>> DeleteProductAsync(Product product)
        {
            var company = Companies.FirstOrDefault(t => t.Id == product.Company.Id);
            company.Products.Remove(product);
            Products.Remove(product);
            Entry(product).State = EntityState.Deleted;
            Audits.Add(CreateAudit(product, "D", string.Empty));
            var result = await CommitAsync().ConfigureAwait(false);
            return new StoreResult<Product>(product, result);
        }

        internal Audit CreateAudit(Company company, string action, string json)
        {
            return new Audit
            {
                Action = action,
                ChangedBy = company.LastUpdatedBy,
                EntityId = company.Id,
                EntityJson = json,
                EntityType = company.GetType().Name.ToUpperInvariant(),
                Timestamp = company.LastUpdated
            };
        }

        internal Audit CreateAudit(Product product, string action, string json)
        {
            return new Audit
            {
                Action = action,
                ChangedBy = product.LastUpdatedBy,
                EntityId = product.Id,
                EntityJson = json,
                EntityType = product.GetType().Name.ToUpperInvariant(),
                Timestamp = product.LastUpdated
            };
        }

        internal async Task<HttpStatusCode> CommitAsync()
        {
            try
            {
                var result = await SaveChangesAsync();
                return result > 0 ? HttpStatusCode.OK : HttpStatusCode.InternalServerError;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                Logger.LogException(ex);
                return HttpStatusCode.Conflict;
            }
            catch (DbUpdateException ex)
            {
                Logger.LogException(ex);
                return HttpStatusCode.InternalServerError;
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>().HasKey(t => t.Id).ForSqlServerIsClustered(false);

            modelBuilder.Entity<Company>().HasIndex(t => t.Name).ForSqlServerIsClustered();

            modelBuilder.Entity<Company>()
                .Property(m => m.Name)
                .HasMaxLength(100)
                .IsRequired();

            modelBuilder.Entity<Company>()
                .Property(m => m.OrgNr)
                .HasMaxLength(48)
                .IsRequired();

            modelBuilder.Entity<Company>()
                .Property(m => m.VatNr)
                .HasMaxLength(48)
                .IsRequired();

            modelBuilder.Entity<Company>()
                .Property(m => m.RowVersion)
                .IsRequired()
                .IsRowVersion();

            modelBuilder.Entity<Company>()
                .Property(m => m.Logo);

            modelBuilder.Entity<Company>()
                .Property(m => m.Created)
                .IsRequired();

            modelBuilder.Entity<Company>()
                .Property(m => m.CreatedBy)
                .IsRequired();

            modelBuilder.Entity<Company>()
                .Property(m => m.LastUpdated)
                .IsRequired();

            modelBuilder.Entity<Company>()
                .Property(m => m.LastUpdatedBy)
                .IsRequired();

            modelBuilder.Entity<Product>().HasIndex(t => t.Name).ForSqlServerIsClustered(true);

            modelBuilder.Entity<Product>()
                .HasOne(p => p.Company)
                .WithMany(b => b.Products)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Product>().HasKey(p => p.Id).ForSqlServerIsClustered(false);

            modelBuilder.Entity<Product>()
                .Property(p => p.Name)
                .HasMaxLength(100)
                .IsRequired();

            modelBuilder.Entity<Product>()
                .Property(m => m.RowVersion)
                .IsRequired()
                .IsRowVersion();

            modelBuilder.Entity<Product>()
                .Property(p => p.Created)
                .IsRequired();

            modelBuilder.Entity<Product>()
                .Property(p => p.CreatedBy)
                .IsRequired();

            modelBuilder.Entity<Product>()
                .Property(p => p.LastUpdated)
                .IsRequired();

            modelBuilder.Entity<Product>()
                .Property(p => p.LastUpdatedBy)
                .IsRequired();

            modelBuilder.Entity<Product>()
                .Property(p => p.Name)
                .HasMaxLength(100)
                .IsRequired();

            modelBuilder.Entity<Audit>().HasKey(p => p.Id);

            modelBuilder.Entity<Audit>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Audit>()
                .Property(a => a.Action)
                .HasMaxLength(1)
                .IsRequired();

            modelBuilder.Entity<Audit>()
                .Property(a => a.ChangedBy)
                .IsRequired();

            modelBuilder.Entity<Audit>()
                .Property(a => a.EntityId)
                .IsRequired();

            modelBuilder.Entity<Audit>()
                .Property(a => a.EntityJson)
                .IsRequired(false);

            modelBuilder.Entity<Audit>()
                .Property(a => a.Timestamp)
                .IsRequired();

            modelBuilder.Entity<Audit>()
                .Property(a => a.EntityType)
                .IsRequired()
                .HasMaxLength(128);
        }
    }
}
