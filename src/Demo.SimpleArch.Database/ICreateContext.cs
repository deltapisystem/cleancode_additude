﻿using Demo.SimpleArch.Data;

namespace Demo.SimpleArch.Database
{
    public interface ICreateContext : IStorageClientFactory
    {
        StorageClient ReadContext();
    }
}
