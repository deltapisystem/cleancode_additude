﻿using System;
using Demo.NotDip.Database;

namespace Demo.NotDip.Business
{
    public class CustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public void CreateCustomerAndDoSomeMoreStuff(Customer customer)
        {
            customer.Id = Guid.NewGuid();
            _customerRepository.CreateCustomer(customer);
        }
    }
}