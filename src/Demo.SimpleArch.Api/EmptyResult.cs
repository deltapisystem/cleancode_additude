﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Demo.SimpleArch.Api
{
    public sealed class EmptyResult : IHttpActionResult
    {
        private readonly HttpStatusCode _statusCode;

        public EmptyResult(HttpStatusCode statusCode)
        {
            _statusCode = statusCode;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(new HttpResponseMessage(_statusCode));
        }
    }
}