﻿using Autofac;
using Demo.SimpleArch.Commands;

namespace Demo.SimpleArch.Api.Composition
{
    public class AutofacCommandHandlerResolver : IResolveCommandHandler
    {
        private readonly IComponentContext _context;

        public AutofacCommandHandlerResolver(IComponentContext context)
        {
            _context = context;
        }

        public IHandleCommand<T> Resolve<T>()
            where T : ICommand
        {
            return _context.ResolveOptional<IHandleCommand<T>>();
        }
    }
}