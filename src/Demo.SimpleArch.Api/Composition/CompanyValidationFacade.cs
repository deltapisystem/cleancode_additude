﻿using System.Net;
using Demo.SimpleArch.Commands;
using Demo.SimpleArch.CreateCompany;

namespace Demo.SimpleArch.Api.Composition
{
    public class CompanyValidationFacade : IVerifyCompany
    {
        public CommandResult VerifyCompany(string orgNr, string vatNr)
        {
            return new CommandResult(HttpStatusCode.OK);
        }

        public void SendCompanyVerificationMail(CreateCompanyCommand model, CommandResult verifyResult)
        {
        }
    }
}