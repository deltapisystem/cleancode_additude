﻿using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace Demo.SimpleArch.Api.Swagger
{
    public class LogoUploadOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.operationId.ToUpperInvariant().Contains("LOGOTOCOMPANY"))
            {
                operation.parameters.Add(new Parameter
                {
                    name = "File",
                    @in = "formData",
                    description = "Upload logo",
                    required = true,
                    type = "file"
                });
                operation.consumes.Add("application/form-data");
            }
        }
    }
}