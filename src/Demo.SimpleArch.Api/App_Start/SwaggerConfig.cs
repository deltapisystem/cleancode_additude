using System;
using System.Web.Http;
using Demo.SimpleArch.Api.Swagger;
using Swashbuckle.Application;
using Swashbuckle.Swagger;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Demo.SimpleArch.Api.Swagger
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.OperationFilter<LogoUploadOperationFilter>();
                        c.SingleApiVersion("v1", "Demo.SimpleArch.Api");
                        c.MapType<DateTimeOffset>(() => new Schema { type = "string", example = DateTimeOffset.UtcNow.ToString(TimeMaster.DefaultUtcFormat) });
                        c.MapType<DateTimeOffset?>(() => new Schema { type = "string", example = "null or " + DateTimeOffset.UtcNow.ToString(TimeMaster.DefaultUtcFormat) });
                        c.MapType<Guid>(() => new Schema { type = "string", example = Guid.NewGuid().ToString() });
                        c.IncludeXmlComments(GetXmlCommentsPath());
                    })
                .EnableSwaggerUi(c =>
                    {
                        c.DocumentTitle("Demo.SimpleArch.Api");
                    });
        }

        private static string GetXmlCommentsPath()
        {
            return $@"{AppDomain.CurrentDomain.BaseDirectory}\bin\Demo.SimpleArch.Api.XML";
        }
    }
}
