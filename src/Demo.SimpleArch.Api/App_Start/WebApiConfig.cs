﻿using System.Globalization;
using System.Net;
using System.Web.Http;
using Demo.SimpleArch.Api.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Demo.SimpleArch.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            DependencyContainerRegistrations.Configure(config);

            // Web API configuration and services
            var settings = config.Formatters.JsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.None;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.Converters.Add(new IsoDateTimeConverter()
            {
                DateTimeStyles = DateTimeStyles.AdjustToUniversal,
                DateTimeFormat = TimeMaster.DefaultUtcFormat
            });
            settings.DateParseHandling = DateParseHandling.DateTimeOffset;
            settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            settings.DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;

            // Web API filter
            var filters = config.Filters;
            filters.Clear();
            filters.Add(new ValidateModelStateAttribute(HttpStatusCode.BadRequest, "The request is invalid."));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });
        }
    }
}
