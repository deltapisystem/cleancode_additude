﻿using System.Configuration;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Demo.SimpleArch.Api.Composition;
using Demo.SimpleArch.Api.Storage;
using Demo.SimpleArch.Commands;
using Demo.SimpleArch.CreateCompany;
using Demo.SimpleArch.Data;
using Demo.SimpleArch.Database;

namespace Demo.SimpleArch.Api
{
    public static class DependencyContainerRegistrations
    {
        public static void Configure(HttpConfiguration config)
        {
            var builder = RegisterToContainerBuilder(config);
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        public static ContainerBuilder RegisterToContainerBuilder(HttpConfiguration config, bool registerControllers = true, bool registerDatabase = true)
        {
            var builder = new ContainerBuilder();
            var logger = new SimpleLogger();

            // instances
            builder.RegisterInstance(logger).As<IAppLogger>();
            builder.RegisterInstance(new TimeMaster()).As<ITimeMaster>();
            builder.RegisterInstance(new DefaultAuditResolver()).As<IAuditResolver>();

            // transient
            builder.RegisterType<CompanyValidationFacade>().As<IVerifyCompany>();

            if (registerControllers)
            {
                builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();
            }

            if (registerDatabase)
            {
                var contextFactory = new ContextFactory(logger, ConfigurationManager.AppSettings["DBConnection"]);
                builder.RegisterInstance(contextFactory)
                    .As<ICreateContext>()
                    .As<IStorageClientFactory>();
            }

            SetUpCommandHandlersAndDispatcher(builder);

            builder.RegisterWebApiFilterProvider(config);
            return builder;
        }

        private static void SetUpCommandHandlersAndDispatcher(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(ICommand))).AsClosedTypesOf(typeof(IHandleCommand<>)).PropertiesAutowired(PropertyWiringOptions.PreserveSetValues);
            builder.RegisterType<AutofacCommandHandlerResolver>().As<IResolveCommandHandler>().SingleInstance();
            builder.RegisterType<CommandHandlerDispatcher>().As<ICommandDispatcher>().PropertiesAutowired(PropertyWiringOptions.PreserveSetValues).SingleInstance();
        }
    }
}