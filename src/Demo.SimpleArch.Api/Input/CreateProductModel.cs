﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Input
{
    public class CreateProductModel
    {
        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}