﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Input
{
    public class UpdateProductModel
    {
        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("rowVersion")]
        public byte[] RowVersion { get; set; }
    }
}