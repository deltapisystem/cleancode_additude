﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Input
{
    public class CreateCompanyModel
    {
        [StringLength(100)]
        [Required(AllowEmptyStrings = false)]
        [JsonProperty("name")]
        public string Name { get; set; }

        [StringLength(48)]
        [Required(AllowEmptyStrings = false)]
        [JsonProperty("vatNr")]
        public string VatNr { get; set; }

        [StringLength(48)]
        [Required(AllowEmptyStrings = false)]
        [JsonProperty("orgNr")]
        public string OrgNr { get; set; }

        [JsonProperty("logoBase64Encoded")]
        public byte[] Logo { get; set; }
    }
}