﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Input
{
    public class DeleteCompanyModel
    {
        [Required]
        [JsonProperty("rowVersion")]
        public byte[] RowVersion { get; set; }
    }
}