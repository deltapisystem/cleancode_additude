﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Input
{
    public class PageFilter
    {
        public const int MaxTake = 1000;

        public PageFilter()
            : this(0, 50)
        {
        }

        public PageFilter(int skip = 0, int take = 50)
        {
            Skip = skip;
            Take = take;
        }

        [JsonProperty("skip")]
        [Range(0, int.MaxValue)]
        public int Skip { get; set; }

        [JsonProperty("take")]
        [Range(1, MaxTake)]
        public int Take { get; set; }
    }
}