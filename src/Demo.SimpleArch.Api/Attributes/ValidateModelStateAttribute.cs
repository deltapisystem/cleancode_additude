﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Demo.SimpleArch.Api.Attributes
{
    public sealed class ValidateModelStateAttribute : ActionFilterAttribute
    {
        public ValidateModelStateAttribute(HttpStatusCode statusCode, string errorMessage)
        {
            StatusCode = statusCode;
            ErrorMessage = errorMessage;
        }

        public HttpStatusCode StatusCode { get; }

        public string ErrorMessage { get; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid)
            {
                return;
            }

            var error = new
            {
                message = ErrorMessage,
                error = actionContext.ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage))
            };
            actionContext.Response = actionContext.Request.CreateResponse(StatusCode, error);
        }
    }
}