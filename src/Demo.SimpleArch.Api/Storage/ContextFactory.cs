﻿using System;
using Demo.SimpleArch.Data;
using Demo.SimpleArch.Database;
using Microsoft.EntityFrameworkCore;

namespace Demo.SimpleArch.Api.Storage
{
    public class ContextFactory : ICreateContext
    {
        private readonly string _connectionString;
        private readonly IAppLogger _logger;

        public ContextFactory(IAppLogger logger, string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            _connectionString = connectionString;
            _logger = logger;
        }

        public StorageClient ReadContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<StorageClient>();
            optionsBuilder.UseSqlServer(_connectionString);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            return new StorageClient(_logger, optionsBuilder.Options);
        }

        public StorageClient WriteContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<StorageClient>();
            optionsBuilder.UseSqlServer(_connectionString);
            return new StorageClient(_logger, optionsBuilder.Options);
        }

        public IStorageClient CreateStorageClient()
        {
            return WriteContext();
        }
    }
}