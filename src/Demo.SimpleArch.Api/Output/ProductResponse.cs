﻿using System;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Output
{
    public class ProductResponse
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("rowVersion")]
        public byte[] RowVersion { get; set; }

        [JsonProperty("lastUpdated")]
        public DateTimeOffset LastUpdated { get; set; }
    }
}