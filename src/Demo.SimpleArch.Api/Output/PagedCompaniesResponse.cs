﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Output
{
    public class PagedCompaniesResponse
    {
        public PagedCompaniesResponse()
        {
            Companies = new List<CompanyListItem>();
            HasMoreResult = false;
        }

        [JsonProperty("companies")]
        public List<CompanyListItem> Companies { get; set; }

        [JsonProperty("hasMoreResult")]
        public bool HasMoreResult { get; set; }
    }
}