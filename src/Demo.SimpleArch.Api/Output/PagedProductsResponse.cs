﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Output
{
    public class PagedProductsResponse
    {
        public PagedProductsResponse()
        {
            Products = new List<ProductResponse>();
            HasMoreResult = false;
        }

        [JsonProperty("companyId")]
        public Guid CompanyId { get; set; }

        [JsonProperty("products")]
        public List<ProductResponse> Products { get; set; }

        [JsonProperty("hasMoreResult")]
        public bool HasMoreResult { get; set; }
    }
}