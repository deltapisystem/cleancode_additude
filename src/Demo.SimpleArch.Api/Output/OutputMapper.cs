﻿using System.Collections.Generic;
using System.Linq;

namespace Demo.SimpleArch.Api.Output
{
    public static class OutputMapper
    {
        public static CompanyResponse ToDetailedModel(this Company company, string logoUrl)
        {
            var model = new CompanyResponse
            {
                Id = company.Id,
                Name = company.Name,
                OrgNr = company.OrgNr,
                VatNr = company.VatNr,
                RowVersion = company.RowVersion,
                LastUpdated = company.LastUpdated,
                Products = company.Products.ToDetailedModel().ToList(),
                LogoUrl = logoUrl
            };

            return model;
        }

        public static IEnumerable<ProductResponse> ToDetailedModel(this IEnumerable<Product> products)
        {
            return products.Select(product => product.ToDetailedModel());
        }

        public static ProductResponse ToDetailedModel(this Product product)
        {
            return new ProductResponse
            {
                Id = product.Id,
                LastUpdated = product.LastUpdated,
                Name = product.Name,
                RowVersion = product.RowVersion
            };
        }
    }
}