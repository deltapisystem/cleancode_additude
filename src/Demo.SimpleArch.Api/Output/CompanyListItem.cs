﻿using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Output
{
    public class CompanyListItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("numberOfProducts")]
        public int NumberOfProducts { get; set; }

        [JsonProperty("logoUrl")]
        public string LogoUrl { get; set; }

        [JsonIgnore]
        public bool HasLogo { get; set; }
    }
}