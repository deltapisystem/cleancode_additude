﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Demo.SimpleArch.Api.Output
{
    public class CompanyResponse
    {
        public CompanyResponse()
        {
            Products = new List<ProductResponse>();
            Name = string.Empty;
            LogoUrl = string.Empty;
            OrgNr = string.Empty;
            VatNr = string.Empty;
        }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("logoUrl")]
        public string LogoUrl { get; set; }

        [JsonProperty("orgNr")]
        public string OrgNr { get; set; }

        [JsonProperty("vatNr")]
        public string VatNr { get; set; }

        [JsonProperty("lastUpdated")]
        public DateTimeOffset LastUpdated { get; set; }

        [JsonProperty("rowVersion")]
        public byte[] RowVersion { get; set; }

        [JsonProperty("products")]
        public IList<ProductResponse> Products { get; set; }
    }
}