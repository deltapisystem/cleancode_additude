﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Demo.SimpleArch.Api.Input;
using Demo.SimpleArch.Api.Output;
using Demo.SimpleArch.CreateCompany;
using Demo.SimpleArch.Database;
using Demo.SimpleArch.DeleteCompany;
using Demo.SimpleArch.UpdateCompany;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.Swagger.Annotations;

namespace Demo.SimpleArch.Api.Controllers
{
    public class CompanyController : BaseController
    {
        private readonly ICreateContext _context;

        public CompanyController(ICreateContext contextCreator)
        {
            _context = contextCreator;
        }

        /// <summary>
        /// Returns companies as list items, simple paging so client can't load "everything"...
        /// </summary>
        [SwaggerResponse(HttpStatusCode.OK, "Returns paged companies. Sorted by name asc as default. Default page size (take) is 100. Max take is 1000.", typeof(PagedCompaniesResponse))]
        [HttpGet]
        [Route("v1/companies", Name = "GetCompanies")]
        public async Task<IHttpActionResult> GetPagedCompanies([FromUri] PageFilter page)
        {
            if (page == null)
            {
                page = new PageFilter();
            }

            var result = new PagedCompaniesResponse();
            using (var context = GetReadContext())
            {
                var pagedResult = await context.Companies
                    .AsNoTracking()
                    .OrderBy(t => t.Name)
                    .Skip(page.Skip)
                    .Take(page.Take + 1)
                    .Select(c => new CompanyListItem
                    {
                        Id = c.Id.ToString(),
                        Name = c.Name,
                        HasLogo = c.Logo != null,
                        NumberOfProducts = c.Products.Count
                    })
                    .ToListAsync();

                result.Companies = pagedResult;
                if (pagedResult.Count > page.Take)
                {
                    result.HasMoreResult = true;
                    result.Companies.RemoveAt(pagedResult.Count - 1);
                }

                foreach (var listItem in result.Companies.Where(t => t.HasLogo))
                {
                    listItem.LogoUrl = CreateUrlToCompanyLogo(listItem.Id);
                }
            }

            return Ok(result);
        }

        /// <summary>
        /// Return company by id.
        /// </summary>
        [SwaggerResponse(HttpStatusCode.OK, "Returns company with all products", typeof(CompanyResponse))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Returns 204 if resource was not found.")]
        [HttpGet]
        [Route("v1/companies/{id}/", Name = "GetCompany")]
        public async Task<IHttpActionResult> GetCompany([FromUri] Guid id)
        {
            var item = await GetCompanyWithProductsAsync(id);
            return item == null ? FromStatusCode(HttpStatusCode.NoContent) : Ok(item.ToDetailedModel(CreateUrlToCompanyLogo(item)));
        }

        /// <summary>
        /// Create a company with name (max 100 char) and logo (not validated at all...)
        /// </summary>
        [SwaggerResponse(HttpStatusCode.Created, "Returns status code 201 and the created entity", typeof(CompanyResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Returns 400 if model is not valid")]
        [HttpPost]
        [Route("v1/companies", Name = "CreateCompany")]
        public async Task<IHttpActionResult> CreateCompany([FromBody] CreateCompanyModel model)
        {
            var command = new CreateCompanyCommand
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                OrgNr = model.OrgNr,
                VatNr = model.VatNr,
                Logo = model.Logo
            };

            var result = await CommandDispatcher.ExecuteAsync(command).ConfigureAwait(false);
            if (result.StatusCode.IsCreated())
            {
                var created = await GetCompanyWithProductsAsync(command.Id);
                return Created(CreateUrlToCompany(command.Id), created.ToDetailedModel(CreateUrlToCompanyLogo(created)));
            }

           return ErrorResult(result.StatusCode, result.ErrorMessage);
        }

        /// <summary>
        /// Update company name (not logo here, use logo endpoint.
        /// </summary>
        [SwaggerResponse(HttpStatusCode.OK, "Returns status code 200 if the entity was updated.")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Returns 204 if resource was not found.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Returns status code 409 if specified rowversion doesn't match.")]
        [HttpPut]
        [Route("v1/companies/{id}/", Name = "UpdateCompany")]
        public async Task<IHttpActionResult> UpdateCompany([FromUri] Guid id, [FromBody] UpdateCompanyModel model)
        {
            var command = new UpdateCompanyCommand
            {
                Id = id,
                Name = model.Name,
                RowVersion = model.RowVersion
            };

            var result = await CommandDispatcher.ExecuteAsync(command).ConfigureAwait(false);
            return result.StatusCode == HttpStatusCode.OK ? Ok() : FromStatusCode(result.StatusCode);
        }

        /// <summary>
        /// Delete a company. You must load the latest version in order to delete the entity (rowVersion).
        /// </summary>
        [SwaggerResponse(HttpStatusCode.OK, "Returns status code 200 if the entity was deleted.")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Returns 204 if resource was not found.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Returns status code 409 if specified rowversion doesn't match.")]
        [HttpDelete]
        [Route("v1/companies/{id}/", Name = "DeleteCompany")]
        public async Task<IHttpActionResult> DeleteCompany([FromUri] Guid id, [FromBody] DeleteCompanyModel model)
        {
            var command = new DeleteCompanyCommand
            {
                Id = id,
                RowVersion = model.RowVersion
            };

            var result = await CommandDispatcher.ExecuteAsync(command).ConfigureAwait(false);
            return result.StatusCode == HttpStatusCode.OK ? Ok() : FromStatusCode(result.StatusCode);
        }

        internal string CreateUrlToCompany(Guid id)
        {
            return Url.Link("GetCompany", new { id });
        }

        internal string CreateUrlToCompanyLogo(Company company)
        {
            return company.HasLogo() ? CreateUrlToCompanyLogo(company.Id.ToString()) : string.Empty;
        }

        internal string CreateUrlToCompanyLogo(string id)
        {
            return Url.Link("GetCompanyLogo", new { id });
        }

        private StorageClient GetReadContext()
        {
            return _context.ReadContext();
        }

        private async Task<Company> GetCompanyWithProductsAsync(Guid id)
        {
            using (var context = GetReadContext())
            {
                return await context.Companies.AsNoTracking().Include(t => t.Products)
                    .FirstOrDefaultAsync(t => t.Id == id).ConfigureAwait(false);
            }
        }
    }
}