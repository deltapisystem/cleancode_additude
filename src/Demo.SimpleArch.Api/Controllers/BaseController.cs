﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Demo.SimpleArch.Commands;
using Swashbuckle.Swagger.Annotations;

namespace Demo.SimpleArch.Api.Controllers
{
    [SwaggerResponseRemoveDefaults]
    public class BaseController : ApiController
    {
        public ICommandDispatcher CommandDispatcher { get; set; }

        public ITimeMaster TimeMaster { get; set; }

        internal IHttpActionResult FromStatusCode(HttpStatusCode statusCode)
        {
            if (statusCode == HttpStatusCode.Conflict)
            {
                return Conflict();
            }

            if (statusCode == HttpStatusCode.BadRequest)
            {
                return BadRequest();
            }

            if (statusCode == HttpStatusCode.InternalServerError)
            {
                return InternalServerError();
            }

            return new EmptyResult(statusCode);
        }

        internal IHttpActionResult ErrorResult(HttpStatusCode statusCode, string errorMessage)
        {
            return ResponseMessage(Request.CreateErrorResponse(statusCode, errorMessage));
        }
    }
}