﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Demo.SimpleArch.Database;
using Demo.SimpleArch.UpdateCompanyLogo;
using Microsoft.EntityFrameworkCore;

namespace Demo.SimpleArch.Api.Controllers
{
    public class LogoController : BaseController
    {
        private readonly ICreateContext _contextCreator;

        public LogoController(ICreateContext contextCreator)
        {
            _contextCreator = contextCreator;
        }

        /// <summary>
        /// Dummy uploaded of logo for the company. No validation that it's an image (or png image).
        /// </summary>
        [HttpPut]
        [Route("v1/companies/{id}/logo", Name = "UpdateCompanyLogo")]
        public async Task<IHttpActionResult> LogoToCompany([FromUri] Guid id)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var logoResult = await GetPngLogoFromRequest();
            if (logoResult == null)
            {
                return BadRequest("Not a valid png image.");
            }

            var commandResult = await CommandDispatcher.ExecuteAsync(new UpdateCompanyLogoCommand
            {
                Image = logoResult.Logo,
                ManufactureId = id
            });

            return FromStatusCode(commandResult.StatusCode);
        }

        /// <summary>
        /// Images, static content, should probably not go through the asp.net request pipeline, instead fetched from cdn/web server. Anyway - this endpoint fetches logo from database end returns it as media type image/png.
        /// Endpoint can't be used from Swagger, instead use the url from browser to get the image or the Base64 endpoint...
        /// </summary>
        [HttpGet]
        [Route("v1/companies/{id}/logo", Name = "GetCompanyLogo")]
        public async Task<IHttpActionResult> GetLogo([FromUri] Guid id)
        {
            Company item;
            using (var context = _contextCreator.ReadContext())
            {
                item = await context.Companies.AsNoTracking()
                    .FirstOrDefaultAsync(t => t.Id == id).ConfigureAwait(false);
            }

            if (item == null || item.Logo.LongLength == 0)
            {
                return FromStatusCode(HttpStatusCode.NoContent);
            }

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(item.Logo)
            };

            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

            return ResponseMessage(response);
        }

        /// <summary>
        /// Returns the image as an base64 encoded string (not a base64 encoded png...).
        /// </summary>
        [HttpGet]
        [Route("v1/companies/{id}/logo/base64", Name = "GetCompanyLogoBase64")]
        public async Task<IHttpActionResult> GetLogoAsBase64String([FromUri] Guid id)
        {
            Company item;
            using (var context = _contextCreator.ReadContext())
            {
                item = await context.Companies.AsNoTracking()
                    .FirstOrDefaultAsync(t => t.Id == id).ConfigureAwait(false);
            }

            if (item == null || item.Logo.LongLength == 0)
            {
                return FromStatusCode(HttpStatusCode.NoContent);
            }

            return Ok(Convert.ToBase64String(item.Logo));
        }

        private async Task<LogoUploadResult> GetPngLogoFromRequest()
        {
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            var file = provider.Contents.FirstOrDefault();
            if (file == null)
            {
                return null;
            }

            if (file.Headers.ContentDisposition.FileName.ToUpperInvariant().EndsWith("PNG"))
            {
                return null;
            }

            return new LogoUploadResult
            {
                Logo = await file.ReadAsByteArrayAsync(),
                FileName = file.Headers.ContentDisposition.FileName.Trim('\"')
            };
        }

        private class LogoUploadResult
        {
            public byte[] Logo { get; set; }

            public string FileName { get; set; }
        }
    }
}