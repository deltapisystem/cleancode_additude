﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Demo.SimpleArch.Database;
using Swashbuckle.Swagger.Annotations;

namespace Demo.SimpleArch.Api.Controllers
{
    public class SystemController : BaseController
    {
        private readonly ICreateContext _context;

        public SystemController(ICreateContext contextCreator)
        {
            _context = contextCreator;
        }

        [SwaggerResponse(HttpStatusCode.OK, "Create default database")]
        [HttpPost]
        [Route("v1/system/createdatabase", Name = "CreateDatabase")]
        public async Task<IHttpActionResult> CreateDatabase()
        {
            using (var storageClient = _context.ReadContext())
            {
                await storageClient.Database.EnsureCreatedAsync();
                storageClient.CreateDefaultData(DateTimeOffset.UtcNow, 100);
            }

            return Ok();
        }
    }
}