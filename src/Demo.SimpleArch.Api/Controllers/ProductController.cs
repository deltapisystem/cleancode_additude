﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Demo.SimpleArch.Api.Input;
using Demo.SimpleArch.Api.Output;
using Demo.SimpleArch.CreateProduct;
using Demo.SimpleArch.Database;
using Demo.SimpleArch.DeleteProduct;
using Demo.SimpleArch.UpdateProduct;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.Swagger.Annotations;

namespace Demo.SimpleArch.Api.Controllers
{
    public class ProductController : BaseController
    {
        private readonly ICreateContext _context;

        public ProductController(ICreateContext contextCreator)
        {
            _context = contextCreator;
        }

        /// <summary>
        /// Returns products for a company, paged.
        /// </summary>
        [SwaggerResponse(HttpStatusCode.OK, "Returns all products", typeof(PagedProductsResponse))]
        [HttpGet]
        [Route("v1/companies/{id}/products", Name = "GetPagedProductsForCompany")]
        public async Task<IHttpActionResult> GetProductsForCompany([FromUri] Guid id, [FromUri] PageFilter page)
        {
            if (page == null)
            {
                page = new PageFilter();
            }

            var pagedResult = new PagedProductsResponse();

            using (var context = GetReadClient())
            {
                var query = from m in context.Companies
                    join p in context.Products on m.Id equals p.Company.Id
                    orderby p.Name
                    where m.Id == id
                    select new ProductResponse
                    {
                        Id = p.Id,
                        Name = p.Name,
                        LastUpdated = p.LastUpdated,
                        RowVersion = p.RowVersion
                    };
                pagedResult.Products = await query.Skip(page.Skip).Take(page.Take + 1).ToListAsync();
            }

            if (pagedResult.Products.Count > page.Take)
            {
                pagedResult.HasMoreResult = true;
                pagedResult.Products.RemoveAt(pagedResult.Products.Count - 1);
            }

            pagedResult.CompanyId = id;
            return Ok(pagedResult);
        }

        /// <summary>
        /// Create a product under a company
        /// </summary>
        [SwaggerResponse(HttpStatusCode.Created, "Returns status code 201 and the created entity.", typeof(ProductResponse))]
        [SwaggerResponse(HttpStatusCode.NoContent, "Returns 204 if company doesn't exists.")]
        [HttpPost]
        [Route("v1/companies/{companyId}/products/", Name = "CreateProduct")]
        public async Task<IHttpActionResult> CreateProduct([FromUri] Guid companyId, [FromBody] CreateProductModel model)
        {
            var command = new CreateProductCommand
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                CompanyId = companyId
            };

            var result = await CommandDispatcher.ExecuteAsync(command).ConfigureAwait(false);
            if (result.StatusCode.IsCreated())
            {
                using (var client = GetReadClient())
                {
                    var createdItem = await client.Products.AsNoTracking()
                        .FirstOrDefaultAsync(t => t.Id == command.Id).ConfigureAwait(false);

                    return Created(CreateUrlToProduct(companyId, command.Id), createdItem.ToDetailedModel());
                }
            }

            return FromStatusCode(result.StatusCode);
        }

        /// <summary>
        /// Get a product for the specified company.
        /// </summary>
        [SwaggerResponse(HttpStatusCode.Created, "Returns status code 200 and the entity")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Returns 204 if company doesn't exists.")]
        [HttpGet]
        [Route("v1/companies/{companyId}/products/{productId}/", Name = "GetProduct")]
        public async Task<IHttpActionResult> GetProduct([FromUri] Guid companyId, [FromUri] Guid productId)
        {
            Product product;
            using (var client = GetReadClient())
            {
                product = await client.Products.Include(t => t.Company).AsNoTracking()
                    .FirstOrDefaultAsync(t => t.Id == productId).ConfigureAwait(false);
            }

            if (product == null || product.Company.Id != companyId)
            {
                return new EmptyResult(HttpStatusCode.NoContent);
            }

            return Ok(product.ToDetailedModel());
        }

        /// <summary>
        /// Update a product.
        /// </summary>
        [SwaggerResponse(HttpStatusCode.OK, "Returns status code 200 if the entity was updated.")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Returns 204 if resource was not found.")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Returns status code 409 if specified rowversion doesn't match.")]
        [HttpPut]
        [Route("v1/companies/{companyId}/products/{productId}/", Name = "UpdateProduct")]
        public async Task<IHttpActionResult> UpdateProduct([FromUri] Guid companyId, [FromUri] Guid productId, [FromBody] UpdateProductModel model)
        {
            var command = new UpdateProductCommand
            {
                ProductId = productId,
                CompanyId = companyId,
                Name = model.Name,
                RowVersion = model.RowVersion
            };

            var result = await CommandDispatcher.ExecuteAsync(command).ConfigureAwait(false);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                return Ok();
            }

            return FromStatusCode(result.StatusCode);
        }

        /// <summary>
        /// Delete a product
        /// </summary>
        [SwaggerResponse(HttpStatusCode.OK, "Returns status code 200 if the entity was deleted.")]
        [SwaggerResponse(HttpStatusCode.NoContent, "Returns 204 if resource was not found.")]
        [HttpDelete]
        [Route("v1/companies/{companyId}/products/{productId}/", Name = "DeleteProduct")]
        public async Task<IHttpActionResult> DeleteProduct([FromUri] Guid companyId, [FromUri] Guid productId)
        {
            var command = new DeleteProductCommand
            {
                CompanyId = companyId,
                ProductId = productId
            };

            var result = await CommandDispatcher.ExecuteAsync(command).ConfigureAwait(false);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                return Ok();
            }

            return FromStatusCode(result.StatusCode);
        }

        internal string CreateUrlToProduct(Guid companyId, Guid productId)
        {
            return Url.Link("GetProduct", new { companyId, productId });
        }

        private StorageClient GetReadClient()
        {
            return _context.ReadContext();
        }
    }
}