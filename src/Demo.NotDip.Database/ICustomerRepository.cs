﻿

using System;

namespace Demo.NotDip.Database
{
    public interface ICustomerRepository
    {
        void CreateCustomer(Customer customer);

        Customer GetCustomer(Guid customerId);

        void DeleteCustomer(Guid customerId);

        void UpdateCustomer(Customer customer);
    }
}
