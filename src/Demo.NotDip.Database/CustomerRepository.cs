﻿using System;
using System.Data.SqlClient;
using Dapper.Contrib.Extensions;

namespace Demo.NotDip.Database
{
    public class CustomerRepository : ICustomerRepository
    {
        public void CreateCustomer(Customer customer)
        {
            using (var con = CreateConnection())
            {
                con.Insert(customer);
            }
        }

        public Customer GetCustomer(Guid customerId)
        {
            using (var con = CreateConnection())
            {
                return con.Get<Customer>(customerId);
            }
        }

        public void DeleteCustomer(Guid customerId)
        {
            using (var con = CreateConnection())
            {
                var customer = con.Get<Customer>(customerId);
                con.Delete(customer);
            }
        }

        public void UpdateCustomer(Customer customer)
        {
            using (var con = CreateConnection())
            {
                con.Update(customer);
            }
        }

        private SqlConnection CreateConnection()
        {
            return new SqlConnection();
        }
    }
}
